export default (err, req, res, next) => {
    console.log('Error: ', JSON.stringify(err))
    res.status(err.statusCode || 500).json(err);
  }
  