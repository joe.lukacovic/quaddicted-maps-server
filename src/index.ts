import * as router from 'aws-lambda-router'
import { getMaps } from './resource/htmlScraper'
import { getMapZip } from './resource/mapStore'
import * as db from './database'

const initPromise = db.init({/* db params */ })
  .then(() => getMaps())
  .then(maps => db.addMaps(maps))
  .catch((e) => console.log('Quaddicted Maps API failed to start due to an error: ' + e.message))

export const handler = router.handler({
  proxyIntegration: {
    debug: true,
    routes: [{
      // request-path-pattern with a path variable:
      path: '/api/maps',
      method: 'GET',
      // we can use the path param 'id' in the action call:
      action: (request, context) => {
        return initPromise
          .then(() => db.getMaps())
          .then(maps => ({
            body: JSON.stringify(maps)
          }))
      }
    }, {
      path: '/api/maps/ping',
      method: 'GET',
      action: (request, context) => {
        return initPromise
          .then(() => JSON.stringify({ message: 'pong' }))
      }
    },
    {
      // request-path-pattern with a path variable:
      path: '/api/maps/:id',
      method: 'GET',
      // we can use the path param 'id' in the action call:
      action: (request, context) => {
        return initPromise
          .then(() =>
            db.getMap(request.paths.id)
              .then(map => getMapZip(map.downloadLink)
                .then(({ url, length }) => ({
                  ...map,
                  byteLength: length,
                  downloadLink: url
                })))
          )
          .then(obj => ({
            statusCode: 200,
            headers: {
              'Content-Type': 'application/json',
              'Cache-Control': `max-age=60`,
            },
            body: JSON.stringify(obj)
          }))
          .catch(err => {
            console.log('ERROR ' + JSON.stringify(err))
            return {
              status: 500,
              headers: {},
              body: err.message
            } as any;
          })
        //.catch(err => res.status(404).send('Not Found'))
      }
    }
    ]
  }
})