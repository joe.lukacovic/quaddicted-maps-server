import {IMap} from '../interfaces/IMap'
import {find} from 'ramda'

// replace with db
let mapsDBArray: Array<IMap> = []

export const init = (options: any): Promise<void> => {
  return Promise.resolve()
}

export const addMaps = (maps:IMap[]) : Promise<void> =>  {
  mapsDBArray = maps
  return Promise.resolve()
}

export const getMaps = () : Promise<Array<IMap>> => {
  return Promise.resolve(mapsDBArray)
}

export const getMap = (id) : Promise<IMap> => {
  return Promise.resolve(find(m => m.id === id, mapsDBArray))
}