export interface IMap {
    name: string,
    title: string,
    author: string,
    id: string,
    downloadLink: string
}