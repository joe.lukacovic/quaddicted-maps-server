var AWS = require('aws-sdk');
import axios from 'axios'
import { promisify } from 'util';
import {IMap} from '../interfaces/IMap'
import {JSDOM} from 'jsdom'
import * as crypto from 'crypto'
import {parseString} from 'xml2js'
import {head, tail, path} from 'ramda'
import moment = require('moment');

const indexFileName = "indexPayload.json"

const putToBucket = (s3, fileName, fileData) => {
  console.log(`copying ${fileName} to bucket ${fileData.length} bytes` )
  var params = {
    Body: fileData, 
    Bucket: process.env.MAPS_BUCKET_NAME, 
    Key: fileName
  } 
  return promisify(s3.putObject.bind(s3))(params)
}

const getFromBucket = (s3, fileName) => {
  var params = {
    Bucket: process.env.MAPS_BUCKET_NAME, 
    Key: fileName
  }
  return promisify(s3.getObject.bind(s3))(params)
}

const getMapReqs = path(['techinfo', 'requirements', 'file'])
const parse = ([dbData, rawHtml]): IMap[] => {
  const dom = new JSDOM(rawHtml);
  const document = dom.window.document
  const rows = [...document.querySelectorAll('#spmaps tr')]
  const fileDb = dbData.files.file

  return rows
    .filter(row => {
      return !!row.querySelector('td.title')
    })
    .map(row => {
      const tds = [...row.querySelectorAll('td')]
      const titleSplit = tds[1].textContent.split(' - ')
      const fileName = head(titleSplit)
      const title = tail(titleSplit).join(' - ')
      const dbEntry = fileDb.find(entry => entry.title === title)
      const name = fileName.substr(0, fileName.indexOf('.'))

      if (!dbEntry) {
        console.log('No db entry found for map ' + title)
        return null
      }
      const reqs = [].concat(getMapReqs(dbEntry)).filter(f => !!f).map(f => f.$.id)
      const date = tds[3].textContent.split('.')
      try {
        return {
          author: tds[0].textContent,
          name: 'asdf',
          title,
          fileName,
          size: tds[2].textContent,
          detailLink: tds[1].querySelector('a').attributes['href'].value,
          downloadLink: tds[2].querySelector('a').attributes['href'].value,
          date: new Date(date[2], date[1], date[0]),
          rating: tds[4].textContent.length,
          userrating: tds[6].textContent.split(' ')[0],
          mapList: [].concat(dbEntry.techinfo.startmap || name)
            .map(m => m.split('/').reverse()[0]), // remove any sub dirs
          requirements: reqs,
          id: crypto.createHash('md5').update(title + tds[2].textContent).digest("hex")
        }
      }
      catch(err) {
        return null
      }
    })
    .filter(map => !!map)
}

const xmlToJson = str => 
  new Promise((resolve, reject) => {
    return parseString(str,
      {explicitArray: false},
      (err, result) => err ? reject(err) : resolve(result))
  })

const generateIndex = () : Promise<IMap[]> => 
  Promise.all([
    axios.get(process.env.MAPS_DB_URL).then(r => xmlToJson(r.data)),
    axios.get(process.env.MAPS_HTML_URL).then(r => r.data)
  ])
  .then(parse)

const generateAndPut = (s3) => 
  generateIndex()
    .then(data => putToBucket(s3, indexFileName, JSON.stringify(data))
      .catch(err => console.log(err.message))
      .then(() => data))

export const getMaps = () : Promise<IMap[]> => {
  const s3 = new AWS.S3();
  return getFromBucket(s3, indexFileName)
    .then(resp => {
      const lastModified = resp.LastModified
      console.log("last modified: " + lastModified)
      if (moment(lastModified, 'ddd MMM DD YYYY HH:mm:ss ZZ').isBefore(moment().subtract(1, 'days'))) {
        return generateAndPut(s3)
      }
      return JSON.parse(resp.Body.toString())
    })
    .catch(err => {
      if (err.statusCode === 404) {
        return generateAndPut(s3)
      }
      throw err
    })
}