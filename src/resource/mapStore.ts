var AWS = require('aws-sdk');
import { promisify } from 'util';
import axios from 'axios'

const copyToBucket = (s3, fileName, fileData) => {
  console.log(`copying ${fileName} to bucket ${fileData.length} bytes` )
  var params = {
    Body: fileData, 
    Bucket: process.env.MAPS_BUCKET_NAME, 
    Key: fileName
  };
  return promisify(s3.putObject.bind(s3))(params)
}
const fileExists = (s3, fileName) => {
  var params = {
    Bucket: process.env.MAPS_BUCKET_NAME, 
    Key: fileName 
   };
   return promisify(s3.headObject.bind(s3))(params)
}

const getFromSource = url => {
  const fullUrl = `${process.env.MAPS_DOWNLOAD_URL_BASE}${url}`
  return  axios({
    method: "get",
    url: fullUrl,
    responseType: 'arraybuffer'
  })
}

export const getMapZip = async (url) => {
  const s3 = new AWS.S3();
  const urlSplit = url.split('/')
  const fileName = urlSplit[urlSplit.length - 1]

  return fileExists(s3, fileName)
    .then(data => data.ContentLength)
    .catch((err) => {
      if (err.statusCode === 404) {
        console.log('File doesnt exist')
        return getFromSource(url)
          .then(resp => copyToBucket(s3, fileName, resp.data).then(() => resp.data.length))
      } else {
        return Promise.reject(err)
      }
    })
    .then((length) => {
      var params = {Bucket: process.env.MAPS_BUCKET_NAME, Key: fileName, Expires: 60}
      return s3.getSignedUrlPromise('getObject', params)
        .then(url => ({
          url,
          length
        }))
    })
}