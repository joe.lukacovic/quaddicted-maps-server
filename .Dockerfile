FROM node:carbon

RUN mkdir -p /usr/app

WORKDIR /usr/app

COPY package.json /usr/app/

RUN npm install --only=production

# Copy all applicable directories
COPY ./dist /usr/app/dist

EXPOSE 80

# Define environment variable
# ENV NAME World

ENTRYPOINT ["npm", "run", "start"]
